package ru.top.top_bank.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.top.top_bank.entity.dto.response.CurrencyToRubleInfoResponse;
import ru.top.top_bank.service.CurrencyCalculationService;

@RestController
@RequestMapping("/currency")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyCalculationService currencyCalculationService;

    @GetMapping("/rate")
    public ResponseEntity<CurrencyToRubleInfoResponse> getRatesToRuble() {
        return ResponseEntity.ok(currencyCalculationService.getCurrencyInfo());
    }
}
