package ru.top.top_bank.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.top.top_bank.entity.Account;
import ru.top.top_bank.entity.dto.response.AccountInfoResponse;
import ru.top.top_bank.service.AccountService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/account")
@Tag(name = "Accounts", description = "Действия с банковскими счётами")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Operation(
            summary = "Возвращает данные по счёту",
            description = "Получает данные по конкретному счёту по его уникальному идентификатору."
    )
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = AccountInfoResponse.class),
                            mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {
                    @Content(schema = @Schema)
            })
    })
    @GetMapping("/{id}")
    public ResponseEntity<AccountInfoResponse> accountInfo(@PathVariable UUID id) {
        return ResponseEntity.ok(accountService.getInfo(id));
    }


    @Operation(
            summary = "Возвращает данные по всем счетам",
            description = "Получает данные всех счетов из банковской системы"
    )
    @GetMapping("/all")
    public ResponseEntity<List<AccountInfoResponse>> allAccountsInfo() {
        return ResponseEntity.ok(accountService.getAllInfo());
    }
}
