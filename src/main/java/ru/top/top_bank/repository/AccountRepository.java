package ru.top.top_bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.top.top_bank.entity.Account;

import java.util.UUID;

public interface AccountRepository extends JpaRepository<Account, UUID> {
}
