package ru.top.top_bank.entity.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyToRubleInfoResponse {
    private LocalDate date;
    private final String base = "RUB";
    private Map<String, Double> rates;
}
