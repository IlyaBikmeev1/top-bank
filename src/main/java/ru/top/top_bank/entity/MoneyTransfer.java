package ru.top.top_bank.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "money_transfer")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MoneyTransfer {
    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "amount")
    private double amount;

    @Column(name = "date_transfer")
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "account_from", referencedColumnName = "id")
    private Account accountFrom;

    @ManyToOne
    @JoinColumn(name = "account_to", referencedColumnName = "id")
    private Account accountTo;
}
