package ru.top.top_bank.service;

import ru.top.top_bank.entity.dto.response.AccountInfoResponse;

import java.util.List;
import java.util.UUID;

public interface AccountService {
    AccountInfoResponse getInfo(UUID accountId);
    List<AccountInfoResponse> getAllInfo();
}
