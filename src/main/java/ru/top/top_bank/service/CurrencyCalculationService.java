package ru.top.top_bank.service;

import ru.top.top_bank.entity.dto.response.CurrencyToRubleInfoResponse;

public interface CurrencyCalculationService {
    void recalculate();
    CurrencyToRubleInfoResponse getCurrencyInfo();
}
