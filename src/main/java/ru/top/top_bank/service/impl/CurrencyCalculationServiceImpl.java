package ru.top.top_bank.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.top.top_bank.entity.dto.response.CurrencyToRubleInfoResponse;
import ru.top.top_bank.service.CurrencyCalculationService;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class CurrencyCalculationServiceImpl implements CurrencyCalculationService {

    private final Map<String, Double> rates;
    private final RestTemplate restTemplate;
    private final String centralBankUrl;
    private LocalDate dateOfRate;

    public CurrencyCalculationServiceImpl(@Autowired RestTemplate restTemplate,
                                          @Value("${external.central-bank.url}") String centralBankUrl) {
        this.rates = new HashMap<>(Map.of(
                "USD", 0.0,
                "EUR", 0.0
        ));
        this.restTemplate = restTemplate;
        this.centralBankUrl = centralBankUrl;
    }

    @Override
    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.DAYS)
    public void recalculate() {
        ResponseEntity<CurrencyToRubleInfoResponse> response = restTemplate
                .getForEntity(centralBankUrl, CurrencyToRubleInfoResponse.class);

        if(!response.getStatusCode().equals(HttpStatus.OK) || response.getBody() == null) {
            throw new IllegalStateException("Couldn't get rates from central bank!");
        }

        rates.clear();
        rates.putAll(response.getBody().getRates());
        rates.forEach((k, v) -> rates.replace(k, 1.0 / v));

        dateOfRate = response.getBody().getDate();

        log.info("Rates from central bank have been successfully updated!");
    }

    @Override
    public CurrencyToRubleInfoResponse getCurrencyInfo() {
        return CurrencyToRubleInfoResponse
                .builder()
                .date(dateOfRate)
                .rates(new HashMap<>(rates))
                .build();
    }
}
