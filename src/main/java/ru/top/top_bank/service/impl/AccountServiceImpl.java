package ru.top.top_bank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.top.top_bank.entity.Account;
import ru.top.top_bank.entity.dto.response.AccountInfoResponse;
import ru.top.top_bank.exception.ResourceNotFoundException;
import ru.top.top_bank.repository.AccountRepository;
import ru.top.top_bank.service.AccountService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountInfoResponse getInfo(UUID accountId) {
        Account account = accountRepository.findById(accountId).orElseThrow(
                () -> new ResourceNotFoundException (
                        String.format("Account with id %s not found!", accountId))
        );

        return AccountInfoResponse
                .builder()
                .accountId(account.getId())
                .totalMoney(account.getTotalMoney())
                .ownerId(account.getClient().getId())
                .build();
    }

    @Override
    public List<AccountInfoResponse> getAllInfo() {
        return accountRepository.findAll()
                .stream()
                .map(account -> AccountInfoResponse.builder()
                        .accountId(account.getId())
                        .totalMoney(account.getTotalMoney())
                        .ownerId(account.getClient().getId())
                        .build())
                .collect(Collectors.toList());
    }
}
