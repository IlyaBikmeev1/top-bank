package ru.top.top_bank.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.top.top_bank.entity.Account;
import ru.top.top_bank.entity.Client;
import ru.top.top_bank.entity.dto.response.AccountInfoResponse;
import ru.top.top_bank.exception.ResourceNotFoundException;
import ru.top.top_bank.repository.AccountRepository;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    private Account account;
    private AccountInfoResponse infoResponse;
    private Client client;

    @BeforeEach
    void init() {
        client = Client.builder()
                .id(UUID.randomUUID())
                .build();

        account = Account.builder()
                .id(UUID.randomUUID())
                .client(client)
                .totalMoney(23445)
                .build();

        infoResponse = AccountInfoResponse.builder()
                .accountId(account.getId())
                .ownerId(client.getId())
                .totalMoney(account.getTotalMoney())
                .build();
    }

    @Test
    void getInfo_shouldBeSuccessful() {
        Mockito.when(accountRepository.findById(Mockito.any()))
                .thenReturn(Optional.of(account));
        assertEquals(accountService.getInfo(account.getId()), infoResponse);
    }

    @Test
    void getInfo_shouldThrowResourceNotFoundException() {
        Mockito.when(accountRepository.findById(Mockito.any()))
                .thenReturn(Optional.empty());

        Exception ex = assertThrows(ResourceNotFoundException.class,
                () -> accountService.getInfo(account.getId()));

        String expectedMessage = String.format("Account with id %s not found!", account.getId());
        assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    void getAllInfo_shouldBeSuccessful() {
        Mockito.when(accountRepository.findAll())
                .thenReturn(Collections.singletonList(account));

        assertTrue(accountService.getAllInfo().contains(infoResponse));
    }
}
